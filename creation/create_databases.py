from src.utils.db_connector import DBConnector
from mysql import connector
from env_constants import err
import creation.create_tables as tables


def recreate_database(db, db_name):
    try:
        db.execute("DROP DATABASE IF EXISTS {}".format(db_name))
        db.execute("CREATE DATABASE {} DEFAULT CHARACTER SET 'utf8'".format(db_name))
    except connector.Error as e:
        err("Failed creating database: {}".format(e))


def pages():
    db = DBConnector()
    recreate_database(db, 'PAGES')
    tables.create_pages_tables(db)


def main():
    pages()


main()
