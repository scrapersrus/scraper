def create_pages_tables(db):
    db.execute('USE PAGES')
    db.execute('DROP TABLE IF EXISTS AMAZON_CA_BOOKS_RAWHTML')
    db.execute('CREATE TABLE AMAZON_CA_BOOKS_RAWHTML('
               'ISBN10 CHAR(10),'
               'HTML LONGTEXT,'
               'RETRIEVED_DATE DATETIME,'
               'PRIMARY KEY (ISBN10, RETRIEVED_DATE)'
               ')')

