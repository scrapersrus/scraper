from os import path
from sys import stderr


ROOT = path.dirname(path.abspath(__file__))
CONFIGS = path.join(ROOT, 'configs')

# CONFIGS
CONFIG_DATABASES = path.join(CONFIGS, 'databases.cfg')


def err(message, exit_code=1, halt=True):
    print(message, file=stderr)
    if halt:
        exit(exit_code)
