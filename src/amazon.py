from src.utils.db_connector import DBConnector
from src.utils.web_client import get

url = 'https://www.amazon.ca/dp/{}/'
connection = DBConnector('POSTS')
isbns = [
    '0735212171',
    '1455563935',
    '1501126067',
    '0393246434',
    '0812995341',
    '0156034026',
    '0393355551',
    '0316547611',
]

for i in range(1, 20):
    for isbn in isbns:
        html = get(url.format(isbn))
        connection.execute('INSERT INTO AMAZON_CA_BOOKS_RAWHTML (ISBN10, HTML, RETRIEVED_DATE) '
                           'VALUES(%s, %s, NOW() )', (isbn, html,))

connection.db_connection.close()

