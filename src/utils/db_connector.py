from configparser import RawConfigParser
from base64 import b64decode
from mysql import connector
from env_constants import CONFIG_DATABASES, err


class DBConnector:
    db_connection = None
    cursor = None
    result_set = None
    result_rows = None
    row_pointer = None

    def __init__(self, section=None, file=CONFIG_DATABASES):
        parser = RawConfigParser()
        parser.read(file)
        if section is not None and not parser.has_section(section):
            err('Config file is missing section [{}]'.format(section))
        config = parser.defaults() if section is None else dict(parser.items(section))
        config['password'] = b64decode(config['password']).decode('utf-8')
        try:
            self.db_connection = connector.connect(**config)
        except connector.Error as e:
            err(e)
        self.cursor = self.db_connection.cursor(buffered=True)

    def execute(self, query, params=None):
        try:
            self.cursor.execute(query, params)
            self.db_connection.commit()
            self.result_rows = self.cursor.rowcount
            self.row_pointer = 0
        except connector.Error as e:
            err(e)

    def get_row(self):
        self.result_set = self.cursor.fetchone()
        self.row_pointer += 1
        return self.result_set

    def get_all_rows(self):
        self.result_set = self.cursor.fetchall()
        self.row_pointer = self.result_rows
        return self.result_set

    def has_result(self):
        return self.result_rows > 0

    def has_next(self):
        return self.row_pointer + 1 <= self.result_rows
