from urllib.request import Request, urlopen


def get(url):
    req = Request(url)
    req.add_header('user-agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64)'
                                    ' AppleWebKit/537.36 (KHTML, like Gecko) '
                                    'Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.68')
    return urlopen(req).read().decode('utf-8')
